<?php
/*
Plugin Name: QT Banner Manager
Plugin URI: http://www.qantumthemes.com/
Description: Add a sponsor and banner manager to your Wordpress website
Author: QantumThemes
Version: 1.0.7
*/



/* = Configuration
===========================================================*/
define ( 'BM_PLUGIN_DIR',plugins_url( '/' , __FILE__ ));
define ( 'BM_PREFIX', 'qtbanner_' );

include ('inc/post_type_sbm/index.php');


/* = Banners ectraction
===========================================================*/
function extract_banners_from_category($cats=''){//cats is an array
	$return = array();
	// wp_reset_query();
	$qry = 'post_type=superbanner&post_status=publish&posts_per_page=3&orderby=menu_order&cat='.$cats;
	$query2 = new WP_Query($qry);
	$return = array();
	$debug = '';
	while ($query2->have_posts()) : $query2->the_post();
		/**/
		$id = $query2->post->ID;//$query2->$post->ID;//' - '.get_post_meta($query2->$post->ID,BM_PREFIX.'url',true);	
		$debug = $id;
		$imgId=get_post_meta($id, BM_PREFIX.'image',true);
		if(isset($imgId)){
			$image = wp_get_attachment_image_src( $imgId, 'large' );
			$image = $image[0];
			$html_to_output =  '<a target="_blank" rel="external nofollow" data-linkout="'.$id.'" id="superbanner-'.$id.'" class="superbanner" href="'.get_post_meta($id, BM_PREFIX.'url',true).'" style="display:block;'.((get_post_meta($id,BM_PREFIX.'width',true) != '')? 'width:'.get_post_meta($id,BM_PREFIX.'width',true).';' : 'width:100%;').get_post_meta($id,BM_PREFIX.'css',true).'"><img src="'.$image.'" alt="'.((get_post_meta($id,BM_PREFIX.'imagealt',true)!='')? get_post_meta($id,BM_PREFIX.'imagealt',true) : 'Clicca qui').'" style="width:100%" /></a>';
			
			$js = '<script type="text/javascript">
					document.write("'.addslashes(str_replace('\n','',$html_to_output)).'");
					</script>';
			$return[] = $js;
			//$return[] = $html_to_output;
		}
	endwhile;
	wp_reset_postdata();
	return;
	//return $return;
}


/* = Schedule exp
============================================================*/


function sbm_add_scheduling() {
	if ( !wp_next_scheduled( 'sbm_expiration' ) ) {
		wp_schedule_event( time(), 'daily', 'sbm_expiration');
	}
}

function sbm_expiration_action() {

	wp_reset_postdata();
	$args           = array(
		'post_type' => CUSTOM_TYPE_QTBANNER,
		'meta_key' => BM_PREFIX.'date',	
		'meta_compare' => 'LIKE',
		'meta_value' => current_time('Y-m-d') 
	);

	$the_query_meta = new WP_Query( $args );
	global $post;
	while ( $the_query_meta->have_posts() ):
		
		$the_query_meta->the_post();
		setup_postdata( $post );
		$post->post_status = 'draft'; // use any post status
        wp_update_post( $post );	
        wp_reset_postdata();	
	endwhile;
	wp_reset_postdata();


}

add_action('sbm_expiration', 'sbm_expiration_action');
add_action('wp', 'sbm_add_scheduling');



/**
 * Banners widget
 * Author: QantumThemes
*/

add_action( 'widgets_init', 'banners_widget' );
function banners_widget() {
	register_widget( 'Banners_Widget' );
}

class Banners_Widget extends WP_Widget {
	public function __construct() {
		$widget_ops = array( 'classname' => 'bannerswidget', 'description' => __('A widget that displays banners ', 'bannerswidget') );
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'bannerswidget-widget' );
		parent::__construct( 'bannerswidget-widget', __('QT Banners Widget', 'bannerswidget'), $widget_ops, $control_ops );
	}
	public function widget( $args, $instance ) {
		extract( $args );
		echo $before_widget;
		echo $before_title.apply_filters("widget_title", $instance['title']).$after_title; 

		

		$queryArray =  array(
			'post_type' => 'banner',
			'posts_per_page' =>$instance['number'],
			'ignore_sticky_posts' => 1
		   );
		
		if($instance['specificid'] != ''){
			$posts = explode(',',$instance['specificid']);
			$finalarr = array();
			foreach($posts as $p){
				if(is_numeric($p)){
					$finalarr[] = $p;
				}
			};
			$queryArray['post__in'] = $finalarr;
		}
		
		

		if($instance['order'] == 'Random'){
			$queryArray['orderby'] = 'rand';
		}else{
			$queryArray['orderby'] = 'menu_order';
			$queryArray['order'] = 'ASC';
		}
		
		$query = new WP_Query($queryArray);
		 ?>
		 <div class="qt-widget-sponsor qt-card">

	         <div class="row banners thumbnails qw-nomarginbottom">  <!---->
	         <?php
	         $postnumber =0;
	         if ( ! is_admin() ) {
				if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); 
					$id = $query->post -> ID;
					if(has_post_thumbnail()) { 
					  	$postnumber ++;
					  	$class = 's6';
					  	$size = "250";
					  	if($instance['number'] == 1){
					  		$class = 's12';
					  		$size = "650";
					  	}
						?>
			            <div class="col <?php echo esc_attr($class); ?> banner "> 
			            	<?php
			            	$link = get_post_meta($id,BM_PREFIX.'url',true);
			            	$link = get_permalink( $id);
			            	$link = add_query_arg( array('bannerRedirect' => $id), $link );

			            	// $link = get_post_meta( esc_attr($id), BM_PREFIX . 'url', true );


			            	if($link != ''){ ?> <a href="<?php echo esc_url($link); ?>" target="_blank" ><?php }
			                	the_post_thumbnail(array($size,$size),array( 'class' => "img-responsive" ));
			               	if($link != ''){ ?> </a><?php }  ?>
			            </div>

			            <?php if((($postnumber -1) % 2) == 0){ ?> <div class="canc"></div> <?php } ?>
			        	<?php 
			        }

		        endwhile; endif; 
		 		?></div><?php  
	    }
		?>
        </div><!--  -->
        <?php
        wp_reset_postdata();	
		echo $after_widget;
	}

	//Update the widget 
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		//Strip tags from title and name to remove HTML 

		$attarray = array(
				'title',
				'number',
				'specificid',
				'order'
		);
		foreach ($attarray as $a){
			$instance[$a] = strip_tags( $new_instance[$a] );
		}
		return $instance;
	}

	public function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array( 'title' => __('Banners', 'bannerswidget'),
							'number'=> '4',
							'specificid'=> '',
							'order'=> 'Random'
							);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
	 	<h2>General options</h2>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'bannerswidget'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'specificid' ); ?>"><?php _e('Add only specific banners (by post ID, comma separated):', 'bannerswidget'); ?></label>
			<input id="<?php echo $this->get_field_id( 'specificid' ); ?>" name="<?php echo $this->get_field_name( 'specificid' ); ?>" value="<?php echo $instance['specificid']; ?>" style="width:100%;" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e('Quantity:', 'number'); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" style="width:100%;" />
		</p>

		<label for="<?php echo $this->get_field_id( 'showcover' ); ?>"><?php _e('Order', 'bannerswidget'); ?></label><br />			
           Random   <input type="radio" name="<?php echo $this->get_field_name( 'order' ); ?>" value="Random" <?php if($instance['order'] == 'Random'){ echo ' checked= "checked" '; } ?> />  
           Page Order  <input type="radio" name="<?php echo $this->get_field_name( 'order' ); ?>" value="Page Order" <?php if($instance['order'] == 'Page Order'){ echo ' checked= "checked" '; } ?> />  
		</p>  

	<?php

	}
}



/*
*
*	Do the post type redirect 
*
===========================================================*/
/* = This is used to serve json contents
=======================================================================*/
if(!function_exists('qw_bannerRedirect')){
function qw_bannerRedirect(){
	global $post;
	if(!is_numeric($_GET['bannerRedirect'])) {
		return;
	}
	$url = get_post_meta(intval(esc_attr($_GET['bannerRedirect'])), BM_PREFIX . 'url', true);
	// die($url);
	if($url != ''){
		$actualclicks = get_post_meta($_GET['bannerRedirect'], BM_PREFIX.'clicks_out', true);
		update_post_meta($post->ID, BM_PREFIX.'clicks_out', $actualclicks+1, $actualclicks);
		wp_redirect( $url , 301 ); exit;
		die($url);
	}
}}
if(isset($_GET['bannerRedirect'])){
	add_action('get_header','qw_bannerRedirect');
}




