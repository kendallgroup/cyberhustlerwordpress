/**
 * @package QT Loveit
 * Script for the Qantumthemes Love It plugin
 */


(function($) {
    "use strict";
    $.fn.qtLoveitInit = function(){
        var post_id, heart;
        $("body").on("click","a.qt-loveit-link", function(e){
            e.preventDefault();
            heart = $(this);
            // Retrieve post ID from data attribute
            post_id = heart.data("post_id");
            // Ajax call
            $.ajax({
                type: "post",
                url: ajax_var.url,
                data: "action=post-like&nonce="+ajax_var.nonce+"&post_like=&post_id="+post_id,
                success: function(count){
                    // If vote successful
                    if(count != "already")
                    {
                        heart.addClass("qt-disabled");
                        heart.find(".count").text(count);
                    }
                }
            });
            return false;
        });
    };

    $(document).ready(function() {
        $.fn.qtLoveitInit();
    });


})(jQuery);