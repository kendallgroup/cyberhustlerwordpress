<?php  
/*
Plugin Name: QT LoveIt
Plugin URI: http://qantumthemes.com
Description: Adds a "Love It" link to posts or other contents
Version: 1.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
Text Domain: qt-loveit
Domain Path: /languages
Based on: https://code.tutsplus.com/articles/how-to-create-a-simple-post-rating-system-with-wordpress-and-jquery--wp-24474
*/

$timebeforerevote = 120; // = 2 hours

/**
 * 	constants
 * 	=============================================
 */
if(!defined('QTLI_PLUGIN_ACTIVE')) {
	define('QTLI_PLUGIN_ACTIVE', true);
}
if(!defined('QTLI_BASE_DIR')) {
	define('QTLI_BASE_DIR', dirname(__FILE__));
}
if(!defined('QTLI_BASE_URL')) {
	define('QTLI_BASE_URL', plugin_dir_url(__FILE__));
}

/**
 * 	language files
 * 	=============================================
 */
if(!function_exists('qtli_load_text_domain')){
function qtli_load_text_domain() {
	load_plugin_textdomain( 'qt-loveit', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}}
add_action( 'init', 'qtli_load_text_domain' );

/**
 * 	includes
 * 	=============================================
 */
include(QTLI_BASE_DIR . '/includes/frontend/qtli-shortcode.php');
include(QTLI_BASE_DIR . '/includes/backend/qtli-functions.php');

/**
 * 	hooks
 * 	=============================================
 */
add_action('wp_ajax_nopriv_post-like', 'qtli_post_like');
add_action('wp_ajax_post-like', 'qtli_post_like');

/**
 * 	Enqueue scripts
 * 	=============================================
 */
if(!function_exists('qtli_lenqueue_stuff')){
function qtli_lenqueue_stuff(){
	wp_enqueue_script('qt_loveit_script', QTLI_BASE_URL.'js/qt-loveit.js', array('jquery'), '1.0', true );
	wp_localize_script('qt_loveit_script', 'ajax_var', array(
	    'url' => admin_url('admin-ajax.php'),
	    'nonce' => wp_create_nonce('ajax-nonce')
	));
}}

add_action( 'wp_enqueue_scripts', 'qtli_lenqueue_stuff' );
