��          �      ,      �  3   �  9   �     �            
        )     1  	   J     T     \  <   i  {   �     "  -   )     W  �  e  F   @  G   �     �     �  \   �     O     T     c     x  	   �     �  F   �  N   �     2  (   B     k                                                	            
                  Attention: Invalid recipient in the page's settings Attention: recipient email missing in the page's settings Contact from your website  Email Error First name Go Back I've read and accept the Last name Message Message sent Some fields are missing, please check the data and try again Sorry, because of a technical error is not possible to deliver your message. Please write us manually at our email address. Submit Thank you, we will answer as soon as possible privacy terms Project-Id-Version: QT ContactForm plugin
Report-Msgid-Bugs-To: Translator Name <translations@example.com>
POT-Creation-Date: 2018-07-23 08:42+0200
PO-Revision-Date: 
Last-Translator: 
Language-Team: QantumThemes
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Textdomain-Support: yesX-Generator: Poedit 1.6.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;esc_html_e;esc_html_x:1,2c;esc_html__;esc_attr_e;esc_attr_x:1,2c;esc_attr__;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_x:1,2c;_n:1,2;_n_noop:1,2;__ngettext:1,2;__ngettext_noop:1,2;_c,_nc:4c,1,2
X-Poedit-Basepath: ..
X-Generator: Poedit 2.0.9
X-Poedit-SearchPath-0: .
 Attenzione: Il destinatario non valido nelle impostazioni della pagina Attenzione: destinatario email mancanti nelle impostazioni della pagina Contattare dal tuo sito Web  Email Errore nelle impostazioni del form. Per cortesia contattateci usando un altro metodo. Grazie Nome Torna Indietro Ho letto e accetto i Cognome Messaggio Messaggio inviato Alcuni campi sono mancanti, si prega di controllare i dati e riprovare Spiacenti, a causa di un errore tecnico non è possibile inviare il messaggio. Invia messaggio Grazie, vi risponderemo appena possibile termini della privacy 