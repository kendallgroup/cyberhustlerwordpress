<?php
/**
 * @author QantumThemes
 * Creates admin settings page
 */

/**
 * Create options page
 */
add_action('admin_menu', 'qt_servercheck_create_optionspage');
if(!function_exists('qt_servercheck_create_optionspage')){
	function qt_servercheck_create_optionspage() {
		add_options_page('QT Server Check', 'QT Server Check', 'manage_options', 'qt_servercheck_settings', 'qt_servercheck_options');
	}
}

/**
 *  Main options page content
 */

function qt_servercheck_isCurl(){
    return function_exists('curl_version');
}


/**
 *  Remote server IP
 */
function qt_servercheck__person() {    
	$ipaddress = '';
	if (isset($_SERVER['HTTP_CLIENT_IP']))
		$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_X_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_FORWARDED'];
	else if(isset($_SERVER['REMOTE_ADDR']))
		$ipaddress = $_SERVER['REMOTE_ADDR'];
	else
		$ipaddress = 'UNKNOWN';
	return $ipaddress;
}



if(!function_exists('qt_servercheck_options')){
	function qt_servercheck_options() {
		?>
		<div class="qt-servercheck">
			<?php 
			/**
			 * The results will be cached every 60 seconds to prevent abuses
			 * @var [type]
			 */
			
			//delete_transient( 'qt_servercheck_result' );

			$cached = get_transient( 'qt_servercheck_result' );
			if($cached){
				echo '<h4>Cached results</h4>';
				echo wp_kses_post( $cached );
				return;
			} else {
				ob_start();

				$errors = 0;
				$has_curl = qt_servercheck_isCurl();

				echo '<h1>Configuration details</h1>';
				$ini_configuration = ini_get_all();
				$relevant_fields_and_min_vals = array(
					'max_execution_time' 	=> '90',
					'max_file_uploads'		=> '10',
					'max_input_vars'		=> '1000',
					'post_max_size'			=>	'128',
					'upload_max_filesize'	=> '128'
				);

				?>


				<table class="qt-servercheck__table">
					<tr>
						<th>Module</td>
						<th>Global value</th>
						<th>Local value</th>
						<th>Reccommended value</th>
						<th>Test</th>
					</tr>
					<?php
						foreach($relevant_fields_and_min_vals as $key => $minval){
							$val = $ini_configuration[$key];
							$test = 'FAIL';
							$test_css = 'qt-servercheck__fail';

							$local_value = intval(str_replace('M','', $val['local_value']));
							if($local_value >= intval($minval)){
								$test = 'PASSED';
								$test_css = 'qt-servercheck__success';
							}
							?>
							<tr>
								<td><?php echo esc_html($key); ?></td>
								<td><?php echo esc_html($val['global_value']); ?></td>
								<td><?php echo esc_html($val['local_value']); ?></td>
								<td><?php echo esc_html($minval); ?></td>
								<td class="<?php echo esc_attr( $test_css ); ?>"><?php echo esc_html($test); ?></td>
							</tr>
							<?php
						}
					?>
				</table>



				<?php  

				/**
				 * =============================================================
				 * CURL module test
				 * =============================================================
				 */
				?>
				<div class="qt-servercheck__test">
					<h2>CURL module test</h2>
					<?php
					if(qt_servercheck_isCurl()){
						echo '<h3 class="qt-servercheck__success">PASSED</h3>';
					} else {
						echo '<h3 class="qt-servercheck__fail">FAIL</h3>';
						echo '<p>This is unusual, and means you cannot connect with the external servers. Please contact the support for alternative solutions.</p>';
						return;
					}
					?>
				</div>

				<?php

				/**
				 * 
				 * =============================================================
				 * Server GET
				 * =============================================================
				 * 
				 */				
				?>
				<div class="qt-servercheck__test">
					<h2>Server GET test</h2>
					<?php  
					// HTML
					$url = 'http://qantumthemes.xyz/t2gconnector-comm/connector-proxy/qt-servercheck-get.html';
					$response = wp_remote_get( $url );
					if ( is_wp_error( $response ) ) {
						$error_message = $response->get_error_message();
						echo '<h3 class="qt-servercheck__fail">FAIL</h3>';
					   	echo "<p>Something went wrong: ". wp_kses_post($error_message).'</p>';
					   	echo "<p>You won't be able to use automatic updates and installations. Please check the support section of the manual for an alternative solution</p>";		  
					} else {
						echo '<h3 class="qt-servercheck__success">PASSED</h3>';
						echo "<p>It seems your server can correctly download the plugins from our repository.</p>";	
						echo $response['body'];
					}
					?>
				</div>





				<?php  
				/**
				 * 
				 * =============================================================
				 * Server POST
				 * =============================================================
				 * 
				 */
				?>
				<div class="qt-servercheck__test">

					<h2>Server POST test</h2>
					<?php
					$url = 'http://qantumthemes.xyz/t2gconnector-comm/connector-proxy/qt-servercheck.php';
					$args = array(
						'method'        => 'POST',
						'timeout'       => 45,
						'redirection'   => 5,
						'body'          => array( 
							'website_url' 	=> get_site_url(),
							'request_ip' 	=> qt_servercheck__person(),
						),
					);
					$response = wp_remote_post(  $url , $args );
					if ( is_wp_error( $response ) ) {
						$error_message = $response->get_error_message();
						echo '<h3 class="qt-servercheck__fail">FAIL</h3>';
					   	echo "<p>Something went wrong: ". wp_kses_post($error_message).'</p>';
					   	echo "<p>You can still perform the manual installation, please contac our support for the alternative solution. </p>";		  
					} else {
						echo '<h3 class="qt-servercheck__success">PASSED</h3>';
						echo "<p>You can automatically validate your purchase code and update the plugins.</p>";	
						echo $response['body'];
					}
					?>
				</div>


				<?php  
				/**
				 * 
				 * =============================================================
				 * PHP benchmark
				 * =============================================================
				 * 
				 */
				?>
				<div class="qt-servercheck__test">
					<?php  
					echo '<h2>Testing your server performance</h2>';
					require_once 'bench-class/bench-class.php';
					?>
				</div>

				<p>Results will be cached for 20 seconds</p>
				<?php 
				$page =  ob_get_clean();
				set_transient( 'qt_servercheck_result', wp_kses_post($page), 20 ) ;
				echo wp_kses_post($page);
			} // not cached
			?>
		</div>
		<?php  
		return;
	}
}