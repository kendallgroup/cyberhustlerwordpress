<?php

// declaration of the var names saved in the options table of WP
$VDL_varnames=array(	  



                    array('submaticGeneral','General settings'	,'section'),
                    array('vdl_all_label','Label for the "Show All" filter'	,'text'),
					array('',''	,'sectionclose'),


					array('submaticFilters','Filters design'	,'section'),
                    array('vdl_filters_color','Filters text color'	,'color'),
                    array('vdl_filters_bordercolor','Filters border color'	,'color'),
                    array('vdl_filters_bgcolor','Filters background color'	,'color'),
                    array('vdl_filters_bgcolorh','Filters background color hover'	,'color'),
                    array('vdl_filters_bordershape'	,'Filters corner radius (integer between 0 and 50)'		,'number',0,50),
                    array('vdl_filters_fontsize'	,'Filters font size in pixel, (integer between 6 and 30)'		,'number',6,30),
					array('',''	,'sectionclose'),

					array('submaticOverlay','Overlay design'	,'section'),
					array('vdl_overlay_color','Color of the overlay effect'	,'color'),
					array('vdl_overlay_opacity'	,'Opacity of the overlay effect (integer between 0 and 100)'		,'number',0,100),
					array('vdl_title_color','Color of title in the overlay effect'	,'color'),
					array('vdl_tags_color','Color of the tags on hover'	,'color'),
					array('vdl_title_size'	,'Font size of the title in pixel (integer between 12 and 100)'		,'number',12,100),
					array('vdl_title_size_mobile'	,'Font size of the title for small screens in pixel, (integer between 12 and 100)'		,'number',12,100),
					array('vdl_hover_top'	,'Superior margin of the hover effect'		,'number',10,500),
					array('',''	,'sectionclose'),
					/*
					array('submaticPreview','Preview Design'	,'section'),
					array('vdl_preview_background','Color of the background of the preview box'	,'color'),
					array('vdl_preview_text_color','Color of the text of the preview box'	,'color'),
					array('vdl_preview_buttons_color','Color of the buttons of the preview box'	,'color'),
					array('vdl_preview_buttons_color_h','Color of the buttons when mouse is over'	,'color'),
					array('',''	,'sectionclose'),
*/
					array('submaticProsettings','Pro settings'	,'section'),
					array('vdl_loadjquery','Load jQuery (change in case of issues with your theme)'	,'bol'),
					array('vdl_enableswipebox','Enable swipebox to open videos in modal'	,'bol'),
					array('vdl_customcss','Custom CSS'	,'textarea'),
					array('',''	,'sectionclose')

);

?>