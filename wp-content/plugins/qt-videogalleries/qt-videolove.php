<?php
/**
 * Plugin Name: QT Video Galleries
 * Description: Display videos in your Wordpress Theme
 * Version: 1.5
 * Author: QantumThemes
 */


/* = Libraries inclusion
=============================================================*/
include 'settings.php';



/* = CSS and JS Enqueue
=============================================================*/
if(!function_exists('vdlFrontCss')){
function vdlFrontCss(){
	if(!is_admin()){
		wp_enqueue_style( 'vdl_css', plugins_url( '/assets/style.css', __FILE__));
	}
}
}
add_action('wp_enqueue_scripts', 'vdlFrontCss'); 

if(!function_exists('vdlFtontJs')){
function vdlFtontJs() {
	if(!is_admin()){
		if(get_option('vdl_loadjquery')=='1'){
			wp_enqueue_script( 'jquery');
		}
		wp_enqueue_script( 'qtsubpages_quicksand', plugins_url( '/assets/jquery.quicksand.js', __FILE__ ), array('jquery'), '1.0.0', true );
		wp_enqueue_script( 'qtsubpages_easing', plugins_url( '/assets/jquery.easing.1.3.js', __FILE__ ), array('jquery'), '1.0.0', true );
		wp_enqueue_script( 'qtsubpages_csstransform', plugins_url( '/assets/jquery-css-transform.js', __FILE__ ), array('jquery'), '1.0.0', true );
		wp_enqueue_script( 'qtsubpages_jquery-animate-css-rotate-scale', plugins_url( '/assets/jquery-animate-css-rotate-scale.js', __FILE__ ), array('jquery'), '1.0.0', true );
		wp_enqueue_script( 'vdl_subpages_js', plugins_url( '/assets/vdl-main.js', __FILE__ ), array('jquery'), '1.0.0', true );
	}
}
}
add_action( 'wp_enqueue_scripts', 'vdlFtontJs' );

/* = Admin CSS and JS
=============================================================*/
if(!function_exists('vdlCssAdmin')){
function vdlCssAdmin(){
	wp_enqueue_style( 'vdl_css_admin', plugins_url( '/assets/style_admin.css', __FILE__));
}
}
add_action('admin_head', 'vdlCssAdmin'); 

if(!function_exists('vdl_add_adminjs')){
function vdl_add_adminjs() {
		wp_enqueue_script( 'vdl_adminjs', plugins_url( '/assets/adminjs.js', __FILE__ ), array('jquery'), '1.0.0', true );
}
}
add_action( 'admin_enqueue_scripts', 'vdl_add_adminjs' );


/* = Adding specific filters to the video editor
=============================================================*/
add_action('init', 'vdl_register_type'); 
if(!function_exists('vdl_register_type')){ 
function vdl_register_type() {
	$name = 'video';

	$labels = array(
		'name' => __('Video','videolove' ).'s',
		'singular_name' => __('video','videolove' ),
		'add_new' => 'Add New ',
		'add_new_item' => 'Add New '.__('video','videolove' ),
		'edit_item' => 'Edit '.__('video','videolove' ),
		'new_item' => 'New '.__('video','videolove' ),
		'all_items' => 'All '.__('video','videolove' ).'s',
		'view_item' => 'View '.__('video','videolove' ),
		'search_items' => 'Search '.__('video','videolove' ).'s',
		'not_found' =>  'No '.$name.' found',
		'not_found_in_trash' => 'No '.$name.'s found in Trash', 
		'parent_item_colon' => '',
		'menu_name' => __('Video','videolove' ).'s'
	);	
    $args = array(
        'labels' => $labels,
        'singular_label' => __('video'),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'page',
		'has_archive' => true,
		'publicly_queryable' => true,
		'rewrite' => true,
		'menu_position' => 48,
		'query_var' => true,
		'exclude_from_search' => false,
		'can_export' => true,
        'hierarchical' => false,
		'page-attributes' => true,
		'menu_icon' => 'dashicons-video-alt',
        'supports' => array('title', 'thumbnail','editor', 'page-attributes','comments', 'revisions' )

    );  
    register_post_type( 'qtvideo' , $args );	

     $labels = array(
		'name' => __( 'Filters','videolove' ),
		'singular_name' => __( 'Filters','videolove' ),
		'search_items' =>  __( 'Search by filter','videolove' ),
		'popular_items' => __( 'Popular filters','videolove' ),
		'all_items' => __( 'All filters','videolove' ),
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => __( 'Edit filter','videolove' ), 
		'update_item' => __( 'Update filter','videolove' ),
		'add_new_item' => __( 'Add filter','videolove' ),
		'new_item_name' => __( 'New filter','videolove' ),
		'separate_items_with_commas' => __( 'Separate filters with commas','videolove' ),
		'add_or_remove_items' => __( 'Add or remove filters','videolove' ),
		'choose_from_most_used' => __( 'Choose from the most used filters','videolove' ),
		'menu_name' => __( 'Filters','videolove' ),
	); 

	$args = array(
		'hierarchical' => false,
		'labels' => $labels,
		'show_ui' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => true,
		'rewrite' => array( 'slug' => 'filter' ),
	);
	register_taxonomy( 'vdl_filters', 'qtvideo', $args );
}}


function qt_vdl_type_flush() {
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'qt_vdl_type_flush' );


/* = Custom fields (Metabox)
=============================================================*/

include 'add_meta_box.php';


/* = Shortcode
=============================================================*/

if(!function_exists('vdl_childs')){
	function vdl_childs($atts){
		$qtchildatts = $atts;
		extract( shortcode_atts( array(
				'showtags' => "yes",
				'showtitle' => "yes",
				'showfilters' => "yes",
				'showpreview' => "yes",
				'columns' => "3",
				'quantity' => "-1",
				'linktext' => '',
				'defaultimg' => plugins_url( 'assets/default.jpg' , __FILE__ )
			), $atts ) );
		require 'subpages.php';
		return  '<div class="vdl-subpages-container">'.$response.'</div>';
	}
	add_shortcode( 'videolove', 'vdl_childs' );
}



/**
 *  Visual Composer integration
 */


add_action( 'vc_before_init', 'qantumthemes_vc_vdl_childs' );
if(!function_exists('qantumthemes_vc_vdl_childs')){
function qantumthemes_vc_vdl_childs() {
	if(function_exists('vc_map')){
	  	vc_map( array(
		     "name" => __( "Video gallery", "qt-videolove" ),
		     "base" => "videolove",
		     "icon" => get_template_directory_uri(). '/img/qt-logo.png',
		     "description" => __( "Add filterable video gallery", "onair2" ),
		     "category" => __( "Theme shortcodes", "qt-videolove"),
		     "params" => array(
		      	array(
		           "type" => "dropdown",
		           "heading" => __( "Show tags", "qt-videolove" ),
		           "param_name" => "showfilters",
		           'value' => array("yes", "no"),
		           "description" => __( "Add tag filters to the gallery", "qt-videolove" )
		        )
		        ,array(
		           "type" => "dropdown",
		           "heading" => __( "Show titles", "qt-videolove" ),
		           "param_name" => "showtitle",
		           'value' => array("yes", "no"),
		           "description" => __( "Add titles to the overlay", "qt-videolove" )
		        )
		        ,array(
		           "type" => "dropdown",
		           "heading" => __( "Show tags", "qt-videolove" ),
		           "param_name" => "showtags",
		           'value' => array("yes", "no"),
		           "description" => __( "Display the tags of each video", "qt-videolove" )
		        )
		        ,array(
		           "type" => "dropdown",
		           "heading" => __( "Columns number", "qt-videolove" ),
		           "param_name" => "columns",
		           'value' => array("3", "4"),
		           "description" => __( "Number of columns", "qt-videolove" )
		        )
		     )
	  	));
	}
}}






/* = Shortcode
=============================================================*/

if(!function_exists('vdl_lastvideo')){
	function vdl_lastvideo(){
		wp_reset_query();
		global $paged;
		global $post;
		$wpbp = new WP_Query(
        	array( 'post_type' => 'qtvideo', 'posts_per_page' =>1, 'post_status'=>'publish') 
		); 
		$wp_query = $wpbp;
		if ($wpbp->have_posts()) : while ($wpbp->have_posts()) : $wpbp->the_post(); 
	        $pageTitle = get_the_title();
	        $postId = get_the_ID();
	        $videoUrl = get_post_meta( $post->ID, '_videolove_url_key', true );
	        $thumb_id = get_post_thumbnail_id();
	 		$preview_url = wp_get_attachment_image_src($thumb_id,'large', true);
            $imgbig = $preview_url[0];
	        return '<h1>'.$pageTitle.'</h1><a href="'.$videoUrl.'" class="videoloveSwipebox"><img src="'. $imgbig.'" class="img-responsive" style="width:100%;height:auto;"></a>';
        endwhile; endif;
	}
	add_shortcode( 'videolovelast', 'vdl_lastvideo' );
}

?>