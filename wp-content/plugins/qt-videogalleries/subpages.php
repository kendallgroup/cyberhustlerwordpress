<?php
$custom_tax_array = array();
$html_list = '';
$count =0;

wp_reset_query();
global $paged;
global $post;


$paged = get_query_var('paged') ? get_query_var('paged') : 1;
$termfilter = get_term_by( 'slug', get_query_var( 'term' ), 'vdl_filters' );
$filter_name = '';

if(is_object($termfilter)){
	$filter_name = $termfilter->slug;
}

if(!isset($parent) || $parent == ''){
    $parent = $post->ID;
}else{
    if ('publish' != get_post_status ( $parent ) ) {
        return;
    }
}

$wpbp = new WP_Query(
            	array(  

            	  //   'post_parent' => $parent, 
                     'post_type' => 'qtvideo',
                   //   'meta_key' => '_thumbnail_id',
                  //   'vdl_filters'=>$filter_name,
                     'posts_per_page' => $quantity,
                     'post_status'=>'publish'
                ) 
); 
$wp_query = $wpbp;

if ($wpbp->have_posts()) : while ($wpbp->have_posts()) : $wpbp->the_post(); 
        $pageTitle = get_the_title();
        $postId = get_the_ID();
        $elementId = 'QTSBelement'.$postId;
        $terms = get_the_terms( $postId, 'vdl_filters' ); 	
		$size = '250';
		$datatype = '';
		if(isset($terms)){           
			if(is_array($terms)){
				foreach ($terms as $term) { 
					$datatype .= "qts".strtolower(preg_replace('/\s+/', '-', $term->slug)).' '; 
					if(!in_array($term->slug,$custom_tax_array)){
						array_push($custom_tax_array, $term->slug);
					}
				}
			}
		}
		$count++;

        if($thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' )){
            $url = $thumb['0'];
            $foundimage = '<img src="'.$url.'"  >';
        }else{
            $foundimage = 'View more >';    
        }

        /*
        *
        * Create terms tags for each post
        *
        */

        $termsArray = get_the_terms( $post->ID, 'vdl_filters' );
        $tarhtml = '';
        
        if(is_array($termsArray)){
            foreach($termsArray as $tar){
                $tarhtml .= '<span class="trm">'.$tar->name.'</span>';
                
            }

        }
        $tarhtml='<p class="trmlist">'.$tarhtml.'</p>';


        /*
        *
        * Create the html
        *
        */
     
        $img = "";
        if(has_post_thumbnail( $post -> ID )){
            $thumb_id = get_post_thumbnail_id();

            $thumb_url = wp_get_attachment_image_src($thumb_id,'medium', true);
            $img = $thumb_url[0];
            $img = ' data-bgimg="'.$img.'" ';

            $preview ="";// ' data-previewimg="" ';
            if($showpreview == "yes" || $showpreview == ""){
                $preview_url = wp_get_attachment_image_src($thumb_id,'medium', true);
                $imgbig = $preview_url[0];
                $preview = ' data-previewimg="'.$imgbig.'" data-previewtitle="'.$pageTitle.'" ';
                $previewclass=" showpreview ";
            }
            $image = get_the_post_thumbnail( $post->ID, 'medium', array() );
        }else{
            if($defaultimg != ''){
                $img = ' data-bgimg="'.$defaultimg.'" ';
            }   
            $image = '<img src="'.esc_url($defaultimg).'" alt="Click to open">';
        }


        if($columns!= "3" && $columns != "4"  && $columns != "2"){$columns = "3";}
        switch ($columns){
            case "2":
                $cols = "6";
            break;
            case "3":
                $cols = "4";
            break;
            case "4":
            default:
                $cols = "3";
            break;
        }



        /*
        *
        * Link to page if swipebox is not active
        *
        */

        $url = get_permalink($post->ID);
     
        /*
        *
        * Video url  if swipebox is Active
        *
        */

        $videoUrl = get_post_meta( $post->ID, '_videolove_url_key', true );
        $swipebox = '';
        if(get_option( 'vdl_enableswipebox', '1') == '1' && $showpreview == "yes" ){
            $url = $videoUrl;
            $previewclass = ' videoloveSwipebox ';
        }

        /*
        *
        * Add element to the list
        *
        */

		$html_list .='
            <div id="'.$elementId.'" class="vdl-subpages-item fgchild vdlcol-md-'.$cols.'" data-type="all '.$datatype.'" data-id="id-'.$count.'"  > '
            .'<div class="vdl-element" >
                <div class="vdl-elementcontents" >'
                .' <a class="qw-disableembedding vdl-link '.$previewclass.'" '.$preview.' href="'.$url.'" '.$img.'>'
              //  . $image          
                . '<span class="detail">'
                .(($showtitle == "yes")?'<span class="title">'.$pageTitle.'</span>' : "")
                .(($showtags == "yes")? $tarhtml : '' )
                .'</span>'
                .'<div class="blockrestore"></div></a>'
                .'<div class="excerpt">'.get_the_excerpt().'</div>'
            .'</div></div>'
            .'</div>';
endwhile; 

if(!isset($width)) $width = 600 ;
if(!isset($height)) $height = 350 ;
$html_list = '
<!-- ========================== VIDEOS ARCHIVE ===============================-->
<div class="sumrow filterable-grid" data-width="'.$width.'" data-height="'.$height.'" >
'.$html_list.'
</div>';


/*
*
*   Creating the filters
*
*/

$filterslist = '';
if($showfilters != "no"){
    $final_term_array = array();
    $counter = 12;
    $args = array(
        'orderby' => 'count',
        'number' => '30',
        'order'   => 'DESC',
    );

    $terms = get_terms('vdl_filters',$args);

    foreach ($terms as $t){
        if(in_array($t->slug,$custom_tax_array) && $counter>1){
            array_push($final_term_array,array($t->slug,$t->name));
            $counter-=1;
        }
    }
    $term_list = '';
    $filterslist = '';
    if(is_array($final_term_array)){
        echo '<!-- filter array count '.count($final_term_array).'--> ';
        if(count($final_term_array) > 0){
            sort($final_term_array);
            foreach ($final_term_array as $ter) {
                $term_list .= '<li><a href="#" class="qts'. $ter[0] .'">' . $ter[1] . '</a></li>';
            }
            $filterslist .= '<ul class="vdl-subpages-tagcloud filterOptions">
                     <li class="active"><a href="#" class="all" id="vdlsubpagesAll">'.((get_option('vdl_all_label') != '')? get_option('vdl_all_label') :"ALL").'</a></li>'
                     .$term_list.
                    '</ul><div class="canc"></div>';
        }
    }
}
$response = $filterslist.$html_list;
else: 
    $response = __('No videos found.','videolove');
endif;


wp_reset_query();
 wp_reset_postdata();
?>