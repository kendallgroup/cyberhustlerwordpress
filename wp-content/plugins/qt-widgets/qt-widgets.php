<?php  
/*
Plugin Name: QT Widgets
Plugin URI: http://qantumthemes.com
Description: Adds custom widgets to Wordpress
Version: 1.3.5
Author: QantumThemes
Author URI: http://qantumthemes.com
Text Domain: qt-widgets
Domain Path: /languages
*/

/**
 * 	constants
 * 	=============================================
 */
if(!defined('QTWI_PLUGIN_ACTIVE')) {
	define('QTWI_PLUGIN_ACTIVE', true);
}
if(!defined('QTWI_BASE_DIR')) {
	define('QTWI_BASE_DIR', dirname(__FILE__));
}
if(!defined('QTWI_BASE_URL')) {
	define('QTWI_BASE_URL', plugin_dir_url(__FILE__));
}

/**
 *
 *	The plugin textdomain
 * 	=============================================
 */
if(!function_exists('qt_widgets_load_plugin_textdomain')){
function qt_widgets_load_plugin_textdomain() {
	load_plugin_textdomain( 'qt-widgets', FALSE, basename( dirname( __FILE__ ) ) . '/languages' );
}}

add_action( 'plugins_loaded', 'qt_widgets_load_plugin_textdomain' );

/**
 * 	includes
 * 	=============================================
 */
include (QTWI_BASE_DIR . '/widgets/widget-about.php');
include (QTWI_BASE_DIR . '/widgets/widget-archives-list.php');
include (QTWI_BASE_DIR . '/widgets/widget-archive.php');
include (QTWI_BASE_DIR . '/widgets/widget-contacts.php');
include (QTWI_BASE_DIR . '/widgets/widget-onair.php');
include (QTWI_BASE_DIR . '/widgets/widget-upcomingshows.php');
include (QTWI_BASE_DIR . '/widgets/widget-chart.php');