<?php
/*
Package: OnAir2
Description: WIDGET ON AIR SHOW
Version: 0.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
*/
  

add_action( 'widgets_init', 'qantumthemes_onair_widget' );
function qantumthemes_onair_widget() {
	register_widget( 'qantumthemes_Onair_widget' );
}

class qantumthemes_Onair_widget extends WP_Widget {
	/**
	 * [__construct]
	 * =============================================
	 */
	public function __construct() {
		$widget_ops = array( 'classname' => 'qtonairwidget', 'description' => esc_attr__('Display which show is on air by reading the schedule of Wordpress', "qt-widgets") );
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'qtonairwidget-widget' );
		parent::__construct( 'qtonairwidget-widget', esc_attr__('QT On Air show', "qt-widgets"), $widget_ops, $control_ops );
	}
	/**
	 * [widget]
	 * =============================================
	 */
	public function widget( $args, $instance ) {
		extract( $args );

		/**
		 * 
		 * Get group of meta for a post
		 * 	=============================================
		 */
		if(!function_exists('qantumthemes_get_group')){
		function qantumthemes_get_group( $group_name , $post_id = NULL ){
			global $post; 	  
			if(!$post_id){ $post_id = $post->ID; }
			$post_meta_data = get_post_meta($post_id, $group_name, true);  
			return $post_meta_data;
		}}

		echo $before_widget;
		if(array_key_exists("title",$instance)){
			echo $before_title.apply_filters("widget_title", $instance['title'], "qtonairwidget-widget").$after_title; 
		}

		$date = current_time("Y-m-d");
		$current_dayweek = current_time("D");
		$total = 0;
		$tabsArray = array();
		$id_of_currentday = 0;
		$now = current_time("H:i");

		$args = array(
			'post_type' => 'schedule',
			'posts_per_page' => -1,
			'post_status' => 'publish',
			'orderby' => 'menu_order',
			'order'   => 'ASC'
		);

		if(array_key_exists("schedulefilter", $instance)) {
			$qantumthemes_schedulefilter = $instance['schedulefilter'];
		}

		if( isset($qantumthemes_schedulefilter) ) {
			if($qantumthemes_schedulefilter !== ''){
				$args ['tax_query'] = array(
					array(
						'taxonomy' => 'schedulefilter',
						'field'    => 'slug',
						'terms'    => $qantumthemes_schedulefilter
					)
				);
			}
		}
		wp_reset_postdata();

		/**
		 * ====================================================================================================
		 * Update from 2017 September 10
		 * adding week-of-the-month filtering if enabled
		 */
		$week_num = qantumthemes_week_number();
		$qt_execute_week_control = get_theme_mod('QT_monthly_schedule', '0' );
		if(get_theme_mod('QT_monthly_schedule', '0' )){
			$week_num = qantumthemes_week_number();
			$args ['meta_key'] = 'month_week';
			$args ['meta_value'] = $week_num;
			$args['meta_compare'] = 'LIKE';
		}

		/* =========================================== update end ===========================================*/


		$the_query_meta = new WP_Query( $args );

		/**
		 * ====================================================================================================
		 * Update from 2019 October 22
		 * Pre parsing to find the actual day and overrid problem of conflict with week days
		 * @since  3.6.4
		 * ====================================================================================================
		 */
		// Reset
		$id_of_currentday = false; 
		$active = '';
		$maincolor = ' ';
		$tabsArrayTemp["active"] = '';
		// Loop to find current day
		while ( $the_query_meta->have_posts() ):
			$the_query_meta->the_post();
			$post = $the_query_meta->post;
			setup_postdata( $post );

			/**
			 * ====================================================================================================
			 * Update from 2017 September 10
			 * adding week-of-the-month filtering if enabled
			 */
			$can_display_this_schedule = true;
			if($qt_execute_week_control  == '1'){
				$can_display_this_schedule = false;
		 		$schedule_weeks = get_post_meta( $post->ID, 'month_week', true );
		 		if(is_array($schedule_weeks)){
		 			foreach ($schedule_weeks as $w){
		 				if($w == $week_num){
		 					$can_display_this_schedule = true;
		 				}
		 			}
		 		}
		 	}
		 	/* =========================================== update end ===========================================*/

		 	if(true === $can_display_this_schedule){ // check added for 2017-09-10 week schedule update
				$active = '';
				$maincolor = '';
				$total++;
				$tabsArrayTemp = array(
					'name' => $post->post_name,
					'title' => $post->post_title,
					'id' => $post->ID,
					'post' => $post,
					'active' => ''
				);
				$schedule_date = get_post_meta($post->ID, 'specific_day', true);
				$schedule_week_day = get_post_meta($post->ID, 'week_day', true);

				/*
				1. check if is a precise date
				*/
				// reset
				
				if($schedule_date){
					if($schedule_date == $date){
						$id_of_currentday = $post->ID;
						$active = ' active';
						$tabsArrayTemp["active"] = 'active';
						$maincolor = ' maincolor';
					}
				} 
				if(!$id_of_currentday){
					if(is_array($schedule_week_day)){
						foreach($schedule_week_day as $day){ // each schedule can fit multiple days
							
							if(strtolower($day) == strtolower($current_dayweek)){
								$id_of_currentday = $post->ID;
								$active = ' active';
								$maincolor = ' maincolor';
								$tabsArrayTemp["active"] = 'active';
							}
						}
					}
				}
			}

		endwhile;
		/* =========================================== pre-parting - update end ===========================================*/
		

		if($id_of_currentday != 0){
			
			$events= qantumthemes_get_group('track_repeatable', $id_of_currentday);   

			if(is_array($events)){
				$maximum = 1;
				$total = 1;
				foreach($events as $event){ 
					$neededEvents = array('show_id','show_time','show_time_end');
					foreach($neededEvents as $n){
					  if(!array_key_exists($n,$events)){
						  $events[$n] = '';
					  }
					}
					$show_id = $event['show_id'][0];
					// echo $show_id;
					global $post;
					global $show_time;
					global $show_time_end;
					
					$post = get_post($show_id); 
					$show_time = $event['show_time'];
					$show_time_end = $event['show_time_end'];
					if($show_time_end == "00:00"){
						$show_time_end = "24:00";
					}


					if($now < $show_time_end && $total <= $maximum){

						/**
				      	 *
				      	 * 	Custom description
				      	 * 
				      	 */
				      	$custom_desc = get_post_meta($show_id , 'show_incipit', true);
						if($custom_desc == ''){
							$excerpt = $post->post_content;
							$custom_desc = $excerpt;
							$charlength = 160;
							if ( mb_strlen( $excerpt ) > $charlength ) {
								$subex = mb_substr( $excerpt, 0, $charlength - 5 );
								$exwords = explode( ' ', $subex );
								$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
								if ( $excut < 0 ) {
									$custom_desc = mb_substr( $subex, 0, $excut );
								} else {
									$custom_desc = $subex;
								}
							} else {
								$custom_desc = $excerpt;
							}
							$custom_desc .= '[...]';
						}

						$total ++;
						?>
						<div class="qtnowonairwidget qtautoupdate-element qt-widget-onair qt-card aligncenter">
							<?php if (has_post_thumbnail()){ ?>
					        <a href="<?php the_permalink(); ?>">
					            <?php the_post_thumbnail( 'qantumthemes-thumbnail' ); ?>
					        </a>
					     	<?php } ?>
							<h4 class="qt-caption-med">
								<span><?php the_title(); ?></span>
							</h4>
							<h5 class="qt-onair-title">
								<?php echo esc_attr(get_post_meta($post->ID,"subtitle", true)); ?>
							</h5>
							<hr class="qt-spacer-s">
							<p class="qt-ellipsis-3 qt-small">
								<?php echo wp_kses_post($custom_desc); ?>
							</p>
							<hr class="qt-spacer-s">
							<a href="<?php the_permalink(); ?>" class="qt-btn qt-btn-s qt-btn-secondary"><?php echo esc_attr__("Info and episodes", "qt-widgets") ?></a>
						</div>
						<?php
					}
					wp_reset_postdata();
				}//foreach
			} else {
				echo esc_attr__("Sorry, there are no shows scheduled on this day","qt-widgets");
			}
			wp_reset_postdata();
		}
		echo $after_widget;
	}

	/**
	 * [update save the parameters]
	 * =============================================
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$attarray = array(
			'title',
			'schedulefilter',
		);
		foreach ($attarray as $a){
			$instance[$a] = strip_tags( $new_instance[$a] );
		}
		return $instance;
	}

	/**
	 * [form widget parameters form]
	 * =============================================
	 */
	public function form( $instance ) {
		$defaults = array( 
				'title' => esc_attr__('Now on air', "qt-widgets"),
				'schedulefilter' => ''
				);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
	 	<h2><?php echo esc_attr__("Options", "qt-widgets"); ?></h2>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo esc_attr__('Title:', "qt-widgets"); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'schedulefilter' ); ?>"><?php echo esc_attr__('Schedule filter (slug):', "qt-widgets"); ?></label>
			<input id="<?php echo $this->get_field_id( 'schedulefilter' ); ?>" name="<?php echo $this->get_field_name( 'schedulefilter' ); ?>" value="<?php echo $instance['schedulefilter']; ?>" style="width:100%;" />
		</p>
		<p>Please remember that the website can only display show items from midnight to midnight</p>
	<?php
	}
}
