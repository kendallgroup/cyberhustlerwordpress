<?php
/**
 * 
 * @package WordPress
 * @subpackage One Click Demo Import
 * @subpackage onair2
 * @version 1.0.0
 * Settings for the demo import
 * https://wordpress.org/plugins/one-click-demo-import/
 * 
*/

add_filter( 'pt-ocdi/regenerate_thumbnails_in_content_import', '__return_false' );
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );


/**
 * Disable thumbnail generation during import or it takes ages
 */
add_filter( 'pt-ocdi/regenerate_thumbnails_in_content_import', '__return_false' );

/**
 * Customize the popup width
 */
function onair2_ocdi_confirmation_dialog_options ( $options ) {
    return array_merge( $options, array(
        'width'       => 400,
        'dialogClass' => 'wp-dialog',
        'resizable'   => false,
        'height'      => 'auto',
        'modal'       => true,
    ) );
}
add_filter( 'pt-ocdi/confirmation_dialog_options', 'onair2_ocdi_confirmation_dialog_options', 10, 1 );

/**
 * Customize the popup width
 */
function onair2_ocdi_plugin_intro_text( $default_text ) {
    $default_text .= '<h2>Welcome to the onair2 Demo Import.</h2>';
     $default_text .= '<h3>Please remember that <strong>WooCommerce</strong> demos will appear only after installing the WooCommerce plugin.</h3><br><br><br>';

    return $default_text;
}
add_filter( 'pt-ocdi/plugin_intro_text', 'onair2_ocdi_plugin_intro_text' );

/**
 * 
 * ==============================================================
 * Demo import array
 * ==============================================================
 * 
 */

function onair2_ocdi_import_files() {
	$url = 'https://qantumthemes.xyz/t2gconnector-comm/onair2/demodata-20190714-jEbLs0/';
	$demos = array(
		array(
			'import_file_name'           => 'Demo 1',
			'categories'                 => array( 'Classic', 'Dark', 'Institutional' ),
			'import_file_url'            => $url.'demo1/onair2.WordPress.2019-07-18D.xml',
			'import_widget_file_url'     => $url.'demo1/www.qantumthemes.xyz-onair2-wpdemo-widgets.wie',
			'import_customizer_file_url' => $url.'demo1/onair2-child-export.json', // dat extension triggers security restrictions
			'import_notice'              => esc_html__( 'Classic demo for the OnAir2 theme.', 'onair2' ),
			'preview_url'                => 'http://qantumthemes.xyz/onair2/wpdemo/',
			'import_preview_image_url'	 => $url.'demo1/preview.jpg',
		),
		// Demo 1
		array(
			'import_file_name'           => 'Demo 2',
			'categories'                 => array( 'Classic', 'Pop', 'Mag', 'Bright' ),
			'import_file_url'            => $url.'demo2/onair2.WordPress.2019-07-18D.xml',
			'import_widget_file_url'     => $url.'demo2/qantumthemes.xyz-onair2-radio-wordpress-theme-widgets.wie',
			'import_customizer_file_url' => $url.'demo2/onair2-child-export.json', // dat extension triggers security restrictions
			'import_notice'              => esc_html__( 'Pop and bright demo perfect for a young and fresh audience', 'onair2' ),
			'preview_url'                => 'https://qantumthemes.xyz/onair2/radio-wordpress-theme/',
			'import_preview_image_url'	 => $url.'demo2/preview.jpg',
		),
		
	);
	
	if ( class_exists( 'WooCommerce' ) ) {
		// Demo 3, only if WooCommerce is active
		// 
		// ========================================================
		// 
		// THIS DEMO IS A SPECIAL VERSION FROM:
		// https://qantumthemes.xyz/onair2/demo3-installer
		// 
		// ========================================================
		$demos[] = array(
			'import_file_name'           => 'Demo 3',
			'categories'                 => array( 'Mag', 'Bright', 'New', 'Shop'),
			'import_file_url'            => $url.'demo3/onair2.WordPress.2019-07-18-V7.xml',
			'import_widget_file_url'     => $url.'demo3/qantumthemes.xyz-onair2-demo3-installer-widgets-V2.wie',
			'import_customizer_file_url' => $url.'demo3/onair2-child-export-V2.json', // dat extension triggers security restrictions
			'import_notice'              => esc_html__( 'The latest OnAir2 demo with WooCommerce and Give donation elements', 'onair2' ),
			'preview_url'                => 'https://qantumthemes.xyz/onair2/demo3/',
			'import_preview_image_url'	 => $url.'demo3/preview.jpg',
		);
	}
	return $demos;
}
add_filter( 'pt-ocdi/import_files', 'onair2_ocdi_import_files' );





function onair2_ocdi_after_import_setup( $selected_import ) {

	// use the name of the selected import
	$demo_name =  $selected_import['import_file_name'];

	if ( 'Demo 1' === $demo_name ) {
		$primary = get_term_by( 'name', 'Main', 'nav_menu' );
		$secondary = get_term_by( 'name', 'Top menu', 'nav_menu' );
		$footer = get_term_by( 'name', 'Footer', 'nav_menu' );
		$front_page = get_page_by_title( 'OnAir2 Home' );
	}

	if ( 'Demo 2' === $demo_name ) {
		$primary = get_term_by( 'name', 'Main', 'nav_menu' );
		$secondary = get_term_by( 'name', 'Top menu', 'nav_menu' );
		$footer = get_term_by( 'name', 'Footer', 'nav_menu' );
		$front_page = get_page_by_title( 'OnAir2 Home' );
	}

	if ( 'Demo 3' === $demo_name ) {
		$primary = get_term_by( 'name', 'Main', 'nav_menu' );
		$secondary = get_term_by( 'name', 'Top menu', 'nav_menu' );
		$footer = get_term_by( 'name', 'Footer', 'nav_menu' );
		$front_page = get_page_by_title( 'Home 2018' );
	}

	

	/**
	 * 
	 * Set the home
	 * 
	 */
	update_option( 'show_on_front', 'page' );
	update_option( 'page_on_front', $front_page->ID );

	/**
	 * 
	 * Set the menus
	 * 
	 */
	$menus = array();
	if( isset( $primary ) ){
		$menus['primary'] = $primary->term_id;
	}
	if( isset( $secondary ) ){
		$menus['secondary'] = $secondary->term_id;
	}
	if( isset( $footer ) ){
		$menus['footer'] = $footer->term_id;
	}

	if( count( $menus ) >= 1 ){ // If my array has items, set them
		set_theme_mod( 'nav_menu_locations', $menus );
	}

}
add_action( 'pt-ocdi/after_import', 'onair2_ocdi_after_import_setup' );