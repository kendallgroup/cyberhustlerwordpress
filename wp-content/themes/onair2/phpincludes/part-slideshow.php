<?php
/*
Package: OnAir2
Description: SLIDESHOW FULLSCREEN
Version: 0.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
*/
?>
<!-- SLIDESHOW FULLSCREEN ================================================== -->
<div class="qt-slickslider-container">
	<div class="qt-slickslider qt-slickslider-single qt-text-shadow qt-black-bg" data-variablewidth="true" data-arrows="true" data-dots="true" data-infinite="true" data-centermode="true" data-pauseonhover="true" data-autoplay="true" data-arrowsmobile="true" data-centermodemobile="true" data-dotsmobile="false" data-variablewidthmobile="true" >
		<!-- SLIDESHOW ITEM -->
		<div class="qt-slick-opacity-fx qt-item">
			<?php get_template_part (  'phpincludes/part-archive-item-post-hero.php'); ?>
		</div>
		<!-- SLIDESHOW ITEM -->
		<div class="qt-slick-opacity-fx qt-item">
			<?php get_template_part (  'phpincludes/part-archive-item-post-hero.php'); ?>
		</div>
		<!-- SLIDESHOW ITEM -->
		<div class="qt-slick-opacity-fx qt-item">
			<?php get_template_part (  'phpincludes/part-archive-item-post-hero.php'); ?>
		</div>
		<!-- SLIDESHOW ITEM -->
		<div class="qt-slick-opacity-fx qt-item">
			<?php get_template_part (  'phpincludes/part-archive-item-post-hero.php'); ?>
		</div>
	</div>
</div>
<!-- SLIDESHOW FULLSCREEN END ================================================== -->
